use log::{trace};

use crate::card::{Card, CardType};
use crate::opcodes::OpCodes;
use color_eyre::{eyre::Result};
use num_traits::FromPrimitive;

pub struct Machine {
    pub registers: [u64; 64]
}

impl Machine {
    pub fn new() -> Machine {
        Machine {
            registers: [0; 64]
        }
    }

    pub fn dissassemble(&self) -> Result<[OpCodes; 224]> {
        // By default, all opcodes are initalized to 00, or R/S (noop)
        let mut retval = [OpCodes::RS; 224];

        // Program memory if organized starting at register 0x2F down to 0x10.
        // Each 56-bit register contains 7 instructions.  Each instruction is
        // 1 byte.  The opcodes enum contains the known opcodes.  This gives us
        // a total possible program size of 224 instructions.  The instructions
        // are organized within the register LSB to MSB.
        for i in 0 .. 224 {
            let byte_index = i % 7;
            let register_index = (32 - (i / 7)) + 0x0F;
            let register = self.registers[register_index];
            let mask = 0xFFu64 << (byte_index * 8);
            let opcode = (register & mask) >> (byte_index * 8);
            let instruction: Option<OpCodes> = FromPrimitive::from_u64(opcode);
            trace!("i: {}, byte_index: {}, register_index: {:#04x}, mask: {:#016x}, opcode: {:#04x}, instruction: {}",
                i, byte_index, register_index, mask, opcode, instruction.unwrap_or(OpCodes::Unknown));

            retval[i as usize] = instruction.expect(&format!("Disassembling opcode: {:#?}", opcode));
            // retval[i as usize] = instruction.unwrap_or(OpCodes::Unknown);
        }

        Ok(retval)
    }

    pub fn add_card(&mut self, card: &Card) {
        // Figure out what address range this card belongs
        let upper_address = match card.header.card_type {
            CardType::DataCardOne => 0x0F,
            CardType::DataCardTwo => 0x3F,
            CardType::ProgramCardOne => 0x2F,
            CardType::ProgramCardTwo => 0x1F
        };

        // Get a local copy of just the registers, and omit the header and checksum
        let mut records: Vec<u64> = card.records
            .iter()
            .map(|(_, _, r)| {
                // We're only interested in the register contents,
                // not where it is on the card (in time)
                let record_input = r.to_owned() as u64;
                // Finally, the nibbles are actually supposed to be in reverse order
                // in the registers.  So: 0x12345 should be 0x54321.
                let mut record_output = 0u64;
                for i in 0 .. 7 {
                    let value = (record_input & (0xF << (i * 4))) >> (i * 4);
                    record_output += value << ((6 - i) * 4);
                    // trace!("Nibble {} mask {:#010x}, value {:#010x}, output: {:#010x}",
                    //     i,
                    //     (0xF << (i * 4)),
                    //     value,
                    //     record_output)
                }

                record_output
            })
            .collect();

        // Discard the header
        records.remove(0);
        // Discard the checksum
        let _ = records.pop();
            
        // The records on the card are 7-nibbles long (28 bits),
        // and two of them combine to make one 56 bit register.
        // Therefore, there are 32 records, and they will end up
        // in 16 registers.
        for i in 0 .. 16 {
            let register = (records[i * 2] << 28) + (records[i * 2 +1]);
            self.registers[upper_address - i] = register;
        }
    }
}