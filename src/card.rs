use color_eyre::{eyre::eyre, eyre::Result};
use log::{trace, debug};
use serde::{Deserialize};
use std::io::Write;
use std::path::PathBuf;
use std::{fs, fs::File};

#[derive(Debug, Clone, Deserialize, Copy)]
pub struct CSVRow {
    #[serde(rename = "Time [s]")]
    seconds: f64,
    #[serde(rename = "RA")]
    ra: i8,
    #[serde(rename = "RB")]
    rb: i8,
}

#[derive(Debug, Clone, Copy)]
pub enum CardType {
    ProgramCardOne,
    ProgramCardTwo,
    DataCardOne,
    DataCardTwo
}

#[derive(Debug, Clone, Copy)]
pub enum Representation {
    Fixed,
    Science,
    Engineering
}

#[derive(Debug, Clone, Copy)]
pub enum AngleMode {
    Radians,
    Degrees,
    Grad
}

#[derive(Debug, Clone, Copy)]
pub struct CardHeader {
    pub card_type: CardType,
    pub representation: Representation,
    pub digits: u8,
    pub angle_mode: AngleMode,
    pub number_of_cards: u8,
    pub flags: u8
}

impl CardHeader {
    fn from_register(register: i32) -> Result<CardHeader> {
        trace!("Getting headers from {:#08x}", register);
        let representation = match register & 0x0FF00000 {
            0x2200000 => Ok(Representation::Fixed),
            0x0400000 => Ok(Representation::Engineering),
            0x0000000 => Ok(Representation::Science),
            n => {
                Err(eyre!("Invalid number representation found in header: {:#?}", n))
            }
        }?;

        let digits = ((register & 0x000F0000) >> (4 * 4)) as u8;

        let angle_mode = match register & 0x0000F000 {
            0x00002000 => Ok(AngleMode::Grad),
            0x00001000 => Ok(AngleMode::Radians),
            0x00000000 => Ok(AngleMode::Degrees),
            n => {
                Err(eyre!("Invalid angle mode found in header: {:#?}", n))
            }
        }?;

        let flags = ((register & 0x00000F00) >> (2 * 4)) as u8;

        let number_of_cards = match register & 0x000000F0 {
            0x00000010 => Ok(1),
            0x00000000 => Ok(2),
            n => {
                Err(eyre!("Invalid number of cards found in header: {:#?}", n))
            }
        }?;

        let card_type = match register & 0x0000000F {
            0x00000004 => Ok(CardType::ProgramCardTwo),
            0x00000003 => Ok(CardType::ProgramCardOne),
            0x00000002 => Ok(CardType::DataCardTwo),
            0x00000001 => Ok(CardType::DataCardOne),
            n => { 
                Err(eyre!("Invalid card type found in header: {:#?}", n))
            }
        }?;

        let header = CardHeader{
            representation,
            digits,
            angle_mode,
            flags,
            number_of_cards,
            card_type
        };

        Ok(header)
    }
}

#[derive(Debug, Clone)]
pub struct Card {
    pub bitmap: String,
    pub name: String,
    pub header:  CardHeader,
    pub rows: Vec<CSVRow>,
    pub records: Vec<(f64, f64, i32)>,
}

#[derive(Debug, Clone, Copy)]
struct State {
    ra: i8,
    rb: i8
}

impl Card {
    fn states_to_bits(states: &Vec<CSVRow>) -> Result<Vec<(f64, i8)>> {
        #[derive(PartialEq, Debug, Clone, Copy)]
        enum Bit {
            Zero,
            One,
            Invalid,
            None
        }

        impl Bit {
            fn value(&self) -> i8 { if self == &Bit::One {1} else {0} }
        }

        let first = states.first().expect("Unexpected zero len array of rows");
        let state = State {
            ra: first.ra,
            rb: first.rb
        };
        
        let bits: Vec<(f64, Bit)> = states
            .iter()
            .scan(state, |state, &row| {
                // If neither changed, then it's not actually a bit
                let mut retval = Bit::None;

                // We can't have two state transitions at the same time
                if state.ra != row.ra && state.rb != row.rb {
                    retval = Bit::Invalid;
                }

                // RA transition is a One bit
                if state.ra != row.ra {
                    retval = Bit::One;
                }
                
                // RB transition is a Zero bit
                if state.rb != row.rb {
                    retval = Bit::Zero;
                }

                state.ra = row.ra;
                state.rb = row.rb;
                Some((row.seconds, retval))
            })
            .filter(|(s, b)| {
                if b == &Bit::Invalid {
                    panic!("At {} RA and RB transitioned at the same time.\n", s);
                }
                b == &Bit::Zero || b == &Bit::One
            })
            .collect();

        trace!("{:#?}", bits);
        debug!("{} data bits found", bits.len());

        let bits = bits
            .iter()
            .map(|(s, b)| {(s.to_owned(), b.value())})
            .collect();

        Ok(bits)
    }

    fn bits_to_nibbles(bits: &Vec<(f64, i8)>) -> Result<Vec<(f64, f64, i8)>> {
        if bits.len() % 4 != 0 {
            return Err(eyre!(
                "Number of bits in trace {} is not an even number of nibbles! {} remainder",
                bits.len(),
                bits.len() % 4
            ));
        }

        // Collect bits into bundles of 4 (nibbles) and add them to a nibbles array
        let mut nibbles = vec!();
        for nibble_bits in bits.chunks(4) {
            if nibble_bits.len() < 4 {
                break;
            } else {
                let mut nibble = 0i8;
                nibble += nibble_bits[0].1 * 1;
                nibble += nibble_bits[1].1 * 2;
                nibble += nibble_bits[2].1 * 4;
                nibble += nibble_bits[3].1 * 8;
                nibbles.push((nibble_bits[0].0, nibble_bits[3].0, nibble));
            }    
        }
        
        for nibble in &nibbles {
            trace!("{},{},{}", nibble.2, nibble.0, nibble.1);
        }
        debug!("{} nibbles found", nibbles.len());

        Ok(nibbles)
    }

    fn nibbles_to_records(nibbles: &Vec<(f64, f64, i8)>) -> Result<Vec<(f64, f64, i32)>> {
        if nibbles.len() % 7 != 0 {
            return Err(eyre!(
                "Number of nibbles in trace {} is not an even number of words! {} remainder",
                nibbles.len(),
                nibbles.len() % 7
            ));
        }

        // Collect nibbles into bundles of 7 (words) and add them to a words array
        let mut words: Vec<(f64, f64, i32)> = vec!();
        for nibbles in nibbles.chunks(7) {
            if nibbles.len() < 7 { break; }
            let mut word = (nibbles[0].0, nibbles[6].1, 0i32);
            word.2 += (nibbles[0].2 as i32) << (4 * 6);
            word.2 += (nibbles[1].2 as i32) << (4 * 5);
            word.2 += (nibbles[2].2 as i32) << (4 * 4);
            word.2 += (nibbles[3].2 as i32) << (4 * 3);
            word.2 += (nibbles[4].2 as i32) << (4 * 2);
            word.2 += (nibbles[5].2 as i32) << (4 * 1);
            word.2 += (nibbles[6].2 as i32) << (4 * 0);
            words.push(word);
        }
        
        for word in &words {
            trace!("{:#010x}, {}, {}", word.2, word.0, word.1);
        }
        debug!("{} records found", words.len());

        Ok(words)
    }

    fn record_to_array(word: i32) -> [i32; 7] {
        let mut nibbles = [0i32; 7];
        for i in 0 .. 7 {
            nibbles[6-i] = (word >> (i * 4)) & 15i32;
        }
        nibbles
    }

    fn check_checksum(records: &Vec<(f64, f64, i32)>) -> Result<()> {
        // I think we need to make a copy of this 
        let mut records = records.clone();

        // Get the checksum from the card
        let card_checksum = records.pop()
            .expect("Can't checksum an empty array!");
        let card_checksum_array = Card::record_to_array(card_checksum.2);

        // Do the 'sum' in checksum
        let mut comp_checksum = [0i32; 7];
        for word in records {
            let nibbles = Card::record_to_array(word.2);
            for i in 0 .. 7 {
                comp_checksum[i] = nibbles[i] + comp_checksum[i];
            }
        }

        // Perform the carries.  Earlier nibbles' carry to later nibbles...
        let mut carry = 0;
        for i in 0 .. 7 {
            comp_checksum[i] += carry;
            carry = comp_checksum[i] / 16;
            comp_checksum[i] = comp_checksum[i] % 16;
        }

        // Check the checksum
        let mut failed_nibbles = 0i8;
        for i in 0 .. 7 {
            if comp_checksum[i] != card_checksum_array[i] { failed_nibbles += 1};
        }

        if failed_nibbles > 0 {
            let computed_checksum: Vec<(f64, f64, i8)> = comp_checksum
                .iter()
                .map(|n| { (0.0, 0.0, n.to_owned() as i8) })
                .collect();
            let computed_checksum = Card::nibbles_to_records(&computed_checksum)
                .expect("Converting checksum format.");
            return Err(eyre!("Checksum check failed!\n\t{:#0x?} computed,\n\t{:#0x?} expected.", 
                computed_checksum[0].2, 
                card_checksum.2
            ));
        }

        Ok(())
    }
    // Read and process a file
    pub fn from_file(path: &PathBuf) -> Result<Card> {
        let extension = path.extension()
            .expect("Unsupported file type")
            .to_str()
            .expect("Unsupported file type");

        // Get file extension to know what to parse
        match extension {
            "csv" => Card::from_csv_file(path),
            "hpp" => Card::from_hpp_file(path),
            _ => Err(eyre!("Unsupported card file type"))
        }
    }

    fn from_hpp_file(path: &PathBuf) -> Result<Card> {
        let obfuscated_contents = fs::read(path)
            .expect(&format!("Reading from file {:?}", path));

        // The file is "encrypted" by xor'ing the contents by 0x55.
        let contents: String = obfuscated_contents
            .iter()
            .map(|b| {(b ^ 0x55) as char})
            .collect();

        // Split the file into lines
        let lines: Vec<&str> = contents.split('\n').collect();
        
        // Check the magic number
        if !lines[0].contains("NeWe") {
            return Err(eyre!("Unexpected magic number {} != NeWe", lines[0]));
        }

        // Get other metadata
        let bitmap = lines[1].trim().to_owned();
        let name = lines[2].trim().to_owned();

        let nibbles: Vec<&str> = (&lines[24..262]).to_vec();

        let mut records: Vec<(f64, f64, i32)> = vec!();
        for reg in nibbles.chunks(7) {
            let mut record = 0i32;
            for i in 0 .. 7 {
                let string_value = reg[i].trim();
                let number_value: u8 = string_value
                    .parse()
                    .expect(&format!("Unable to convert line \"{}\" to number", string_value));
                record += (number_value as i32) << ((6 - i) * 4);
            }
            
            records.push((0.0, 0.0, record));
        }
        
        let header = CardHeader::from_register(records[0].2)
            .expect("Unable to parse header");

        Card::check_checksum(&records)
            .expect("Checksum validation failed!");

        Ok(Card {
            bitmap,
            name,
            header,
            rows: vec!(),
            records
        })
    }

    fn from_csv_file(path: &PathBuf) -> Result<Card> {
        let name = path
            .file_stem().expect("Getting base filename")
            .to_str().expect("Converting filename to string");

        let contents = fs::read_to_string(path)
            .expect(&format!("Reading from file {:?}", path));

        let mut rdr = csv::ReaderBuilder::new()
            .has_headers(true)
            .from_reader(contents.as_bytes());

        let rows: Vec<CSVRow> = rdr.deserialize()
            .filter_map(|x| x.expect("Deserializing row"))
            .collect();

        // Ensure there are a non-zero number of rows
        if rows.is_empty() {
            return Err(eyre!("Empty file"));
        }

        let bits = Card::states_to_bits(&rows)
            .expect("Converting states to bits");

        let nibbles = Card::bits_to_nibbles(&bits)
            .expect("Collecting bits into nibbles");

        let records = Card::nibbles_to_records(&nibbles)
            .expect("Collecting nibbles into words");

        Card::check_checksum(&records)
            .expect("Checksum validation failed");

        let header = CardHeader::from_register(records[0].2)
            .expect("Computing card header");

        debug!("Card header: {:?}", header);

        Ok(Card {
            bitmap: "blank.bmp".to_owned(),
            name: name.to_owned(),
            header,
            rows,
            records,
        })
    }

    pub fn to_hpp_file(&self, path: &PathBuf) -> Result<()> {
        // File header info (after the byte count)
        let mut output = "67\r\n".to_string();
        output += &format!("{}\r\n", self.bitmap);
        output += &format!("{}\r\n", self.name);

        // Leading zeros
        for _ in 0 .. 21 {
            output += "0\r\n";
        }

        // Data
        for word in &self.records {
            let nibble_array = Card::record_to_array(word.2);
            for i in 0 .. 7 {
                output += &format!("{}\r\n", nibble_array[i]);
            }
        }

        // Do the checksum again :/
        let checksum = &self.records[self.records.len() - 1];
        let nibble_array = Card::record_to_array(checksum.2);
        for i in 0 .. 7 {
            output += &format!("{}\r\n", nibble_array[i]);
        }
        // Create the final file format
        let output = format!("NeWe\r{}\r{}", output.len(), output);
        trace!("HPP file contents: \n{}", output);

        let output: Vec<u8> = output
            .as_bytes()
            .iter()
            .map(|b| {b ^ 0x55})
            // .map(|b| {b.to_owned()})
            .collect();

        let mut file = File::create(path).expect("Opening file for writing");
        file.write(&output).expect("Writing to file");
        file.flush().expect("Flushing file contents");
        
        Ok(())
    }
}

