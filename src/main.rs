
// Logging
use color_eyre::{eyre::Result};
use log::{debug};

// Arguments
use clap::{App, Arg};
use std::{env, path::PathBuf};

#[macro_use]
extern crate num_derive;
mod opcodes;

mod machine;
use machine::Machine;

mod card;
use card::Card;

use crate::opcodes::{KeyCode};

pub fn main() -> Result<()> {
    pretty_env_logger::init();
    color_eyre::install()?;

    let matches = App::new("HP-97 card data processor")
        .version("0.1.0")
        .author("William Dillon <william@housedillon.com>")
        .about("Processes saleae trace CSVs and produces card data binaries")
        .arg(
            Arg::with_name("disassemble")
                .short("d")
                .long("dissassemble")
                .takes_value(false)
                .help("Attempt to disassemble the data into commands"),
        )
        .arg(
            Arg::with_name("quiet")
                .takes_value(false)
                .help("Suppresses progress printing")
                .short("q")
                .long("quiet")
        )
        .arg(
            Arg::with_name("hpp_file")
                .takes_value(false)
                .short("h")
                .long("hpp")
                .help("Outputs a HP emulator HPP file, using the filename of the input")
        )
        .arg(
            Arg::with_name("input")
                .takes_value(true)
                .help("Input Saleae trace csv files")
                .multiple(true)
                .min_values(1)
        )
        .arg(
            Arg::with_name("registers")
                .takes_value(false)
                .help("Print registers contained on the card")
                .short("r")
                .long("registers")
        )
        .get_matches();

    let quiet = matches.is_present("quiet");
    let files: Vec<PathBuf> = matches.values_of("input")
        .expect("No input values provided.")
        .map(|s| { PathBuf::from(s.to_owned()) })
        .collect();

    if !quiet {
        debug!("Using input files: ");
        for file in &files {
            println!("{:?}", &file);
        }
    }

    let cwd = env::current_dir().expect("Getting current working directory");    

    let mut machine = Machine::new();

    for file in &files {
        let name = file
            .file_stem().expect("Getting base filename")
            .to_str().expect("Converting filename to string");

        println!("Processing {}:", name);
        let card = Card::from_file(file).expect("Unable to read card file.");

        if matches.is_present("hpp_file") {
            let mut hpp_output = cwd.clone();
            hpp_output.push(format!("{}.hpp", name));
            card.to_hpp_file(&hpp_output).expect("Writing hpp file");
        }

        machine.add_card(&card);
    }

    if matches.is_present("registers") {
        for (i, register) in machine.registers.iter().enumerate() {
            println!("Register {:#04x}: {:#016x}", i, register);
        }
    }

    if matches.is_present("disassemble") {
        let instructions = machine.dissassemble().expect("Disassembling card");
        for (index, instruction) in instructions.iter().enumerate() {
            let keycode = KeyCode::from(instruction);
            println!("{:03} {} {}", index, instruction, keycode);
        }
    }

    Ok(())
}
